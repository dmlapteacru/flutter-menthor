import 'package:flutter/material.dart';
import 'package:fluttertest/bottom/custom_app_bar/tab_item.dart';

class BottomAppNavBar extends StatefulWidget {
  BottomAppNavBar({
    Key key,
    @required this.items,
    this.currentIndex = 0,
    this.onTap,
    this.iconSize = 20.0,
    this.titleSize = 14.0,
    this.backgroundColor = Colors.white,
    this.colorItems = Colors.black,
    this.colorActiveItem = Colors.red,
    this.height = 50.0,
  })  : assert(items != null),
        assert(items.length >= 2),
        assert(0 <= currentIndex && currentIndex < items.length),
        assert(iconSize != null),
        super(key: key);

  final List<TabAppBarItem> items;

  final int currentIndex;

  final ValueChanged<int> onTap;

  final double iconSize;

  final Color backgroundColor;

  final Color colorItems;

  final Color colorActiveItem;

  final double height;

  final double titleSize;

  @override
  State<StatefulWidget> createState() => _BottomAppNavBarState();
}

///
///
/// State of BottomAppNavBar
///
///
class _BottomAppNavBarState extends State<BottomAppNavBar> {
  int _currentIndex;

  @override
  void initState() {
    super.initState();
    _currentIndex = widget.currentIndex;
  }

  void _updateIndex(int index) {
    widget.onTap(index);
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final FloatingActionButtonLocation fabLocation =
        Scaffold.of(context).widget.floatingActionButtonLocation;

    List<Widget> _items = List.generate(widget.items.length, (int index) {
      return _buildTabItem(widget.items[index], index, _updateIndex);
    });

    return BottomAppBar(
      color: widget.backgroundColor,
      shape: CircularNotchedRectangle(),
      child: Container(
        height: widget.height,
        child: _buildItems(fabLocation, _items),
      ),
    );
  }

  Widget _buildItems(
      FloatingActionButtonLocation fabLocation, List<Widget> items) {
    Widget _defaultItems = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: items,
    );

    if (fabLocation == FloatingActionButtonLocation.centerDocked) {
      _defaultItems = _buildCenterDockedItems(items);
    } else if (fabLocation == FloatingActionButtonLocation.endDocked) {
      _defaultItems = _buildEndDockedItems(items);
    }

    return _defaultItems;
  }

  Widget _buildEndDockedItems(List<Widget> items) {
    return Padding(
      padding: EdgeInsets.only(right: 76.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: items,
      ),
    );
  }

  Widget _buildCenterDockedItems(List<Widget> items) {
    int middleIndex = (items.length / 2).ceil();

    return Row(
      children: <Widget>[
        Container(
          height: widget.height,
          width: MediaQuery.of(context).size.width * 0.5,
          child: Padding(
            padding: EdgeInsets.only(right: 38.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: items.getRange(0, middleIndex).toList(),
            ),
          ),
        ),
        Container(
          height: widget.height,
          width: MediaQuery.of(context).size.width * 0.5,
          child: Padding(
            padding: EdgeInsets.only(left: 38.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: items.getRange(middleIndex, items.length).toList(),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildTabItem(
    TabAppBarItem item,
    int index,
    ValueChanged<int> onPressed,
  ) {
    Color _color =
        _currentIndex == index ? widget.colorActiveItem : widget.colorItems;

    return Expanded(
      child: SizedBox(
        height: widget.height,
        child: InkWell(
          onTap: () => onPressed(index),
          child: Padding(
            padding: const EdgeInsets.only(top: 2.0, bottom: 8.0),
            child: Column(
              mainAxisAlignment: _currentIndex == index ? MainAxisAlignment.start : MainAxisAlignment.end,
//              mainAxisAlignment: MainAxisAlignment.center,
              children: item.title == null
                  ? <Widget>[
                      Icon(item.icon, color: _color, size: widget.iconSize),
                    ]
                  : <Widget>[
                _currentIndex == index ?
                      Icon(item.icon, color: _color, size: widget.iconSize)
                :
                      Icon(item.icon, color: _color, size: widget.iconSize - 4.0),
                _currentIndex == index ?
                Text(
                        item.title,
                        style:
                            TextStyle(color: _color, fontSize: widget.titleSize),
                      )
                :
                Text(
                        item.title,
                        style:
                            TextStyle(color: _color, fontSize: widget.titleSize - 4.0),
                      )
                    ],
            ),
          ),
        ),
      ),
    );
  }
}
