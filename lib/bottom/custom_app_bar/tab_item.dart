import 'package:flutter/material.dart';

class TabAppBarItem {
  const TabAppBarItem({
    @required this.icon,
    this.title,
  }) : assert(icon != null);

  final IconData icon;

  final String title;
}
