import 'package:flutter/material.dart';

class BottomTest extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BottomTest();
  }
}

class _BottomTest extends State<BottomTest> {
  int _currentIndex = 0;
  final List<Widget> _items = [
    PlaceholderWidget(Colors.yellow),
    PlaceholderWidget(Colors.deepOrange),
    PlaceholderWidget(Colors.deepOrange),
    PlaceholderWidget(Colors.deepOrange),
    PlaceholderWidget(Colors.green)
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Text("+"),
      ),
      body: _items[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.airplanemode_active, color: Colors.black,),
            title: Text("TT#1"),
          ), BottomNavigationBarItem(
            icon: Icon(Icons.airplanemode_active,color: Colors.black,),
            title: Text("TT#1"),
          ), BottomNavigationBarItem(
            icon: Icon(Icons.airplanemode_active,color: Colors.black,),
            title: Text("TT#1"),
          ),
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}

class PlaceholderWidget extends StatelessWidget {
  final Color color;

  PlaceholderWidget(this.color);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "DATATATATA",
        style: TextStyle(color: color),
      ),
    );
  }
}
