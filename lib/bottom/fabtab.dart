import 'package:flutter/material.dart';

class FABBottomAppBarItem {
  FABBottomAppBarItem({this.iconData, this.text});
  IconData iconData;
  String text;
}

class FABBottomAppBar extends StatefulWidget {
  final List<FABBottomAppBarItem> items = [];

  @override
  State<StatefulWidget> createState() => FABBottomAppBarState();
}

class FABBottomAppBarState extends State<FABBottomAppBar> {
  int _selectedIndex = 0;

  _updateIndex(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
//    List<Widget> items = List.generate(widget.items.length, (int index) {
//      return _buildTabItem(
//        item: widget.items[index],
//        index: index,
//        onPressed: _updateIndex,
//      );
//    });

    return BottomAppBar(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
//        children: items,
      ),
    );  }

//  Widget _buildTabItem({
//    FABBottomAppBarItem item,
//    int index,
//    ValueChanged<int> onPressed,
//  }) {
//    Color color = _selectedIndex == index ? widget.selectedColor : widget.color;
//    return Expanded(
//      child: SizedBox(
//        height: widget.height,
//        child: Material(
//          type: MaterialType.transparency,
//          child: InkWell(
//            onTap: () => onPressed(index),
//            child: Column(
//              mainAxisSize: MainAxisSize.min,
//              mainAxisAlignment: MainAxisAlignment.center,
//              children: <Widget>[
//                Icon(item.iconData, color: color, size: widget.iconSize),
//                Text(
//                  item.text,
//                  style: TextStyle(color: color),
//                )
//              ],
//            ),
//          ),
//        ),
//      ),
//    );
//  }

}