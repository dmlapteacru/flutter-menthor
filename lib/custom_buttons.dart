import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  CustomButton({@required this.onPressed});

  final GestureTapCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
//                borderRadius: BorderRadius.circular(100.0),
                  boxShadow: [new BoxShadow(
                      color: Colors.grey,
                      blurRadius: 10.0,
                      spreadRadius: 0.2
                  ),]
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: RawMaterialButton(
                  fillColor: Colors.green,
                  splashColor: Colors.red,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0,
                        horizontal: 20.0
                    ),
                    child: Text(
                      "ADD CONTACT",
                      style: TextStyle(
                          color: Colors.white
                      ),
                    ),
                  ),
                  onPressed: onPressed,
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(50.0)),
                  elevation: 0.0,
                  highlightElevation: -2.0,
                ),
              ),
            ),
            MaterialButton(
              child: Text("Material"),
              color: Colors.red,
              onPressed: onPressed,
            ),FlatButton(
              child: Text("Material"),
              color: Colors.red,
              onPressed: onPressed,
            ),OutlineButton(
              child: Text("Material"),
              disabledBorderColor: Colors.yellow,
              highlightedBorderColor: Colors.blue,
              borderSide: BorderSide(color: Colors.black, width: 2.0),
              color: Colors.black,
              onPressed: onPressed,
            ),RaisedButton(
              child: Text("Material"),
              color: Colors.red,
              onPressed: onPressed,
            ),Container(
              decoration: BoxDecoration(
//                  borderRadius: BorderRadius.all(),
                boxShadow: [new BoxShadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    spreadRadius: 1.0
                ),],
//                border: Border.all(color: Colors.green, width: 2.0)
              ),
              child: MaterialButton(
                child: Text("Material"),
                color: Colors.red,
                onPressed: onPressed,
              ),
            ),FloatingActionButton(
              onPressed: onPressed,
              elevation: 2.0,
            )
          ],
        ),
      ),);
  }
}