import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FlexContainers extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
//          padding: EdgeInsets.all(80.0),
          width: MediaQuery.of(context).size.width * 0.5,
          height: 200.0,
          color: Colors.red,
          child: LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints){
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: constraints.maxWidth * 0.3,
                        height: 50.0,
                        color: Colors.yellow,
                      ), Container(
                        width: constraints.maxWidth * 0.1,
                        height: 50.0,
                        color: Colors.green,
                      ), Container(
                        width: constraints.maxWidth * 0.1,
                        height: 50.0,
                        color: Colors.blue,
                      ),
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  ),
                );
              }),
        ),
      ),
    );
  }
}