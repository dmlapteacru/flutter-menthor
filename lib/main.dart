import 'package:flutter/material.dart';
import 'package:fluttertest/bottom/custom_app_bar/bottom_app_nav_bar.dart';
import 'package:fluttertest/bottom/custom_app_bar/tab_item.dart';
import 'package:fluttertest/bottom/default_tab_bottom_nobutton.dart';
import 'package:fluttertest/custom_buttons.dart';
import 'package:fluttertest/flex_containers.dart';
import 'package:fluttertest/post_bloc.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.red,
      ),
      home: new CustomFloatButton(),
    );
  }
}

class CustomFloatButton extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CustomFloatButton();
}

class _CustomFloatButton extends State<CustomFloatButton> {
  int _currentIndex = 0;

  final List<Widget> _items = [
    PlaceholderWidget(Colors.black),
    PlaceholderWidget(Colors.green),
    PlaceholderWidget(Colors.green),
    PlaceholderWidget(Colors.orange),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _items[_currentIndex],
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.backup),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      bottomNavigationBar: BottomAppNavBar(
        onTap: onTapped,
        currentIndex: _currentIndex,
        items: [
          TabAppBarItem(
            icon: Icons.all_inclusive,
            title: "#1",
          ),
          TabAppBarItem(
            icon: Icons.all_inclusive,
            title: "#2",
          ),
          TabAppBarItem(
            icon: Icons.all_inclusive,
            title: "#3",
          ),
          TabAppBarItem(
            icon: Icons.all_inclusive,
            title: "#4",
          )
        ],
      ),
    );
  }

  void onTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
