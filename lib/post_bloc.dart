
import 'dart:async';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';


class PostBloc {
  final _streamController = new PublishSubject<Post>();
  Observable<Post> get outer => _streamController.stream;

  var _counter = 4;

  void dispose(){
    _streamController.close();
  }

  getCounter() async {
    print("COUNTER");
    Post post = new Post();
    Post pp = await post.fetchPost();
    print(pp.id);
    _streamController.sink.add(pp);
  }

  void addInt(){
    _counter+=1;
  }
}


class Post {
  final int userId;
  final int id;
  final String title;
  final String body;

  Post({this.userId, this.id, this.title, this.body});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
      body: json['body'],
    );
  }

  Future<Post> fetchPost() async {
    final response =
    await http.get('https://jsonplaceholder.typicode.com/posts/1');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      print(Post.fromJson(json.decode(response.body)).id);
      return Post.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }
}